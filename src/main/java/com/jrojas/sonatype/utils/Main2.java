package com.jrojas.sonatype.utils;

public class Main2 {

    private static final String ZERO = "zero";
    private static String[] oneToNine = {
            "zero", "one", "two", "three", "four", "five", "six", "seven", "height", "nine"
    };

    private static String[] tenToNinteen = {
            "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
    };

    private static String[] dozens = {
            "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"
    };

    public static String solution(int number) {
        if (number == 0)
            return ZERO;

        return generate(number).trim();
    }

    public static String generate(int number) {
        if (number >= 1000000000) {
            return generate(number / 1000000000) + " billion " + generate(number % 1000000000);
        } else if (number >= 1000000) {
            return generate(number / 1000000) + " million " + generate(number % 1000000);
        } else if (number >= 1000) {
            final int modulo = number % 1000;
            final String s = modulo == 0 ? " thousand " : " thousand and ";
//            return generate(number / 1000) + " thousand " + generate(modulo);
            return generate(number / 1000) + s + generate(modulo);
        } else if (number >= 100) {
            final int modulo = number % 100;
            final String s = modulo == 0 ? " hundred " : " hundred and ";
            return generate(number / 100) + s + generate(number % 100);
        }

        return generate1To99(number);
    }

    private static String generate1To99(int number) {
        if (number == 0)
            return "";

        if (number <= 9)
            return oneToNine[number];
        else if (number <= 19)
            return tenToNinteen[number % 10];
        else {
            final int modulo = number % 10;
            return dozens[number / 10] +  (modulo == 0 ? "" : "-" + generate1To99(modulo));
        }
    }
}


